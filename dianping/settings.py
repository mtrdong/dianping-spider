# Scrapy settings for dianping project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html
from pathlib import Path

# 项目根目录
BASE_DIR = Path(__file__).parent.parent

# 数据目录
DATA_DIR = Path(BASE_DIR, 'data')

BOT_NAME = 'dianping'

SPIDER_MODULES = ['dianping.spiders']
NEWSPIDER_MODULE = 'dianping.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = 'dianping (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 3  # 平均每个请求之间的延迟时间
DOWNLOAD_DELAY_RANDOMIZE = True  # 随机化请求间隔
DOWNLOAD_DELAY_RANGE = (1, 5)  # 随机延迟时间的范围
# The download delay setting will honor only one of:
# CONCURRENT_REQUESTS_PER_DOMAIN = 16
# CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
COOKIES_ENABLED = True

# Disable Telnet Console (enabled by default)
# TELNETCONSOLE_ENABLED = False

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
# }

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
# SPIDER_MIDDLEWARES = {
#    'dianping.middlewares.DianpingSpiderMiddleware': 543,
# }

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy.downloadermiddlewares.cookies.CookiesMiddleware': None,  # 禁用内置 CookiesMiddleware
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,  # 禁用内置 UserAgentMiddleware
    'scrapy.downloadermiddlewares.redirect.RedirectMiddleware': None,  # 禁用内置 RedirectMiddleware
    # 'dianping.middlewares.DianpingDownloaderMiddleware': 543,
    'dianping.middlewares.CustomCookiesMiddleware': 544,
    'dianping.middlewares.CustomUserAgentMiddleWare': 545,
    'dianping.middlewares.CustomRedirectMiddleware': 546,
    'dianping.middlewares.ProxyMiddleware': 547,
}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
# EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
# }

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'dianping.pipelines.DianpingPipeline': 100000,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
# AUTOTHROTTLE_ENABLED = True
# The initial download delay
# AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
# AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
# AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
# AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
# HTTPCACHE_ENABLED = True
# HTTPCACHE_EXPIRATION_SECS = 0
# HTTPCACHE_DIR = 'httpcache'
# HTTPCACHE_IGNORE_HTTP_CODES = []
# HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

# Set settings whose default value is deprecated to a future-proof value
REQUEST_FINGERPRINTER_IMPLEMENTATION = '2.7'
TWISTED_REACTOR = 'twisted.internet.asyncioreactor.AsyncioSelectorReactor'

# 请求失败时重试
RETRY_TIMES = 5  # 最大重试次数
RETRY_HTTP_CODES = [403]  # 要重试的HTTP状态码列表

# 代理池
# 不带验证信息的代理：
# {
#     'proxy': 'http://proxy1.example.com:8888'
# }
# 带有验证信息的代理：
# {
#     'proxy': 'http://proxy2.example.com:8888',
#     'username': 'your_username2',
#     'password': 'your_password2'
# }
PROXY_LIST = []

# 浏览器启动文件
BINARY_LOCATION = 'C:/Program Files (x86)/Microsoft/Edge/Application/msedge.exe'

# 登录链接
LOGIN_URL = 'https://account.dianping.com/pclogin'

# 验证链接
VERIFY_URL = 'https://verify.meituan.com/v2/web/general_page'

# 爬取的城市
CITY_LIST = ['北京', '上海', '广州', '深圳', '成都', '重庆', '杭州', '西安', '武汉', '苏州', '郑州', '南京', '天津', '长沙',
             '东莞', '宁波', '佛山', '合肥', '青岛', '昆明', '沈阳', '济南', '无锡', '厦门', '福州', '温州', '金华', '哈尔滨',
             '大连', '贵阳', '南宁', '泉州', '石家庄', '长春', '南昌', '惠州', '常州', '嘉兴', '徐州', '南通', '太原', '保定',
             '珠海', '中山', '兰州', '临沂', '潍坊', '烟台', '绍兴']

# 爬取的分类
CATEGORY_ITEM = {
    # 一级分类（必须）
    '美食': {
        # 二级分类（可选。为空时则筛选至一级分类）
        '小吃快餐': [
            '快餐简餐',
            '馄饨|抄手|扁食',
            '饺子'
        ]  # 三级分类（可选。为空时则筛选至二级分类）
    }
}

# WOFF字体（动态解析）
WOFF_FONT = {}
