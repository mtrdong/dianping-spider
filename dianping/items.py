# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class CityItem(scrapy.Item):
    name = scrapy.Field()  # 城市名称
    path = scrapy.Field()  # 城市部分Path


class CategoryItem(scrapy.Item):
    name = scrapy.Field()  # 分类名称
    path = scrapy.Field()  # 分类部分Path


class ShopLinkItem(scrapy.Item):
    name = scrapy.Field()  # 店铺名称
    link = scrapy.Field()  # 店铺链接


class ShopInfoItem(scrapy.Item):
    name = scrapy.Field()  # 店铺名称
    address = scrapy.Field()  # 店铺地址
    city = scrapy.Field()  # 所在城市
    shop_power = scrapy.Field()  # 店铺星星
    five_score = scrapy.Field()  # 星星分数
    review_count = scrapy.Field()  # 评价数量
    avg_price = scrapy.Field()  # 消费价格
    shop_score = scrapy.Field()  # 店铺评分
    business_district = scrapy.Field()  # 热门商圈
