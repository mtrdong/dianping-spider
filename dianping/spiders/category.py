import scrapy

from dianping.items import CategoryItem
from dianping.settings import CATEGORY_ITEM
from dianping.utils.xpath import anal_cate1_id, anal_cate2_id, anal_cate3_id, anal_header_data


class CategorySpider(scrapy.Spider):
    """ 获取分类ID，用于构成查询店铺列表的URL路径中的分类部分 """
    name = 'category'
    allowed_domains = ['www.dianping.com']
    start_urls = ['https://www.dianping.com/']

    def parse(self, response, **kwargs):
        """ 解析一级分类 """
        # 从HTML页面中解析所有一级分类的名称和ID
        all_cate = anal_cate1_id(response.text)
        # 获取设置的分类的ID，组成分类部分Path
        for cate_name, child in CATEGORY_ITEM.items():
            cate_id = all_cate.get(cate_name)  # 提取一级分类ID
            if cate_id:
                # 如果一级分类下设有二级分类，则进一步获取二级分类ID
                # 然后和一级分类ID组成分类部分Path
                if child:
                    header_data = anal_header_data(response.text)
                    url = f'{self.start_urls[0]}{header_data["cityEnName"]}/{cate_id}'
                    yield scrapy.Request(
                        url=url,
                        callback=self.parse2,
                        dont_filter=True,
                        cb_kwargs={'cate': child, 'cate1_id': cate_id}
                    )
                # 如果未设置二级分类，则一级分类ID即为分类部分Path
                else:
                    item = CategoryItem()
                    item['name'] = cate_name
                    item['path'] = cate_id
                    yield item

    def parse2(self, response, **kwargs):
        """ 解析二级分类 """
        cate = kwargs['cate']
        cate1_id = kwargs['cate1_id']
        # 从HTML页面中解析所有二级分类的名称和ID
        all_cate = anal_cate2_id(response.text)
        # 获取设置的分类的ID，组成分类部分Path
        for cate_name, child in cate.items():
            cate_id = all_cate.get(cate_name)  # 提取二级分类ID
            if cate_id:
                # 如果二级分类下设有三级分类，则进一步获取三级分类ID
                # 然后和一级分类ID组成分类部分Path
                if child:
                    header_data = anal_header_data(response.text)
                    url = f'{self.start_urls[0]}{header_data["cityEnName"]}/{cate1_id}/{cate_id}'
                    yield scrapy.Request(
                        url=url,
                        callback=self.parse3,
                        dont_filter=True,
                        cb_kwargs={'cate': child, 'cate1_id': cate1_id}
                    )
                # 如果未设置三级分类，则分类部分Path由一级分类ID和二级分类ID组成
                else:
                    item = CategoryItem()
                    item['name'] = cate_name
                    item['path'] = f'{cate1_id}/{cate_id}'
                    yield item

    def parse3(self, response, **kwargs):
        """ 解析三级分类 """
        cate = kwargs['cate']
        cate1_id = kwargs['cate1_id']
        # 从HTML页面中解析所有三级分类的名称和ID
        all_cate = anal_cate3_id(response.text)
        # 获取设置的分类的ID，组成分类部分Path
        for cate_name in cate:
            cate_id = all_cate.get(cate_name)  # 提取三级分类ID
            if cate_id:
                # 类部分Path由一级分类ID和三级分类ID组成
                item = CategoryItem()
                item['name'] = cate_name
                item['path'] = f'{cate1_id}/{cate_id}'
                yield item


if __name__ == '__main__':
    from scrapy.cmdline import execute
    execute(['scrapy', 'crawl', 'category'])
