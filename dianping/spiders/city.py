import scrapy

from dianping.items import CityItem
from dianping.settings import CITY_LIST
from dianping.utils.xpath import anal_city_pinyin


class CitySpider(scrapy.Spider):
    """ 获取城市拼音，用于构成查询店铺列表的URL路径中的城市部分 """
    name = 'city'
    allowed_domains = ['www.dianping.com']
    start_urls = ['https://www.dianping.com/citylist']

    def parse(self, response, **kwargs):
        """ 解析城市拼音 """
        # 解析城市名称和对应的城市拼音
        all_city = anal_city_pinyin(response.text)
        # 提取指定的城市名称和对应的城市拼音
        cities = {city: all_city.get(city) for city in CITY_LIST if all_city.get(city)}
        for city, pinyin in cities.items():
            item = CityItem()
            item['name'] = city
            item['path'] = pinyin
            yield item


if __name__ == '__main__':
    from scrapy.cmdline import execute
    execute(['scrapy', 'crawl', 'city'])
