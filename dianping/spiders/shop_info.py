from urllib.parse import urlencode

import scrapy

from dianping.items import ShopInfoItem
from dianping.utils.common import load_json_data
from dianping.utils.xpath import anal_shop_info, anal_review_star, anal_trade_area


def get_shop_detail_link():
    """ 获取店铺详情查询链接 """
    url_list = []
    for shop in load_json_data('shop_link.jsonl'):
        url_list.append(shop['link'])
    return url_list


class ShopInfoSpider(scrapy.Spider):
    name = 'shop_info'
    allowed_domains = ['www.dianping.com']
    start_urls = get_shop_detail_link()

    def parse(self, response, **kwargs):
        """ 解析店铺基本信息 """
        data = anal_shop_info(response.text)
        if data:
            # 添加热门商圈
            trade_area = anal_trade_area(response.text)
            data.update(trade_area)
            # 拼接获取店铺评价和星级的链接
            ids = data.pop('ids')
            url = 'https://www.dianping.com/ajax/json/shopDynamic/reviewAndStar?' + urlencode(ids)
            yield scrapy.Request(
                url=url,
                callback=self.parse2,
                dont_filter=True,
                cb_kwargs={'data': data}
            )

    def parse2(self, response, **kwargs):
        """ 解析店铺的星级、评论数、费用和评分 """
        data = kwargs['data']
        response_data = response.json()
        review_star = anal_review_star(response_data)
        data.update(review_star)
        item = ShopInfoItem()
        item.update(data)
        yield item


if __name__ == '__main__':
    from scrapy.cmdline import execute
    execute(['scrapy', 'crawl', 'shop_info'])
