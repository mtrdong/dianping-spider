from urllib.parse import urlparse

import scrapy

from dianping.items import ShopLinkItem
from dianping.utils.common import load_json_data
from dianping.utils.xpath import anal_shop_link, get_total


def get_shop_list_link():
    """ 获取店铺列表查询链接 """
    url = 'https://www.dianping.com/{}/{}'
    url_list = []
    for city in load_json_data('city.jsonl'):
        for cate in load_json_data('category.jsonl'):
            url_list.append(url.format(city['path'], cate['path']))
    return url_list


class ShopLinkSpider(scrapy.Spider):
    """ 获取店铺列表，解析店铺链接 """
    name = 'shop_link'
    allowed_domains = ['www.dianping.com']
    start_urls = get_shop_list_link()

    def parse(self, response, **kwargs):
        shops = anal_shop_link(response.text)
        for name, link in shops.items():
            item = ShopLinkItem()
            item['name'] = name
            item['link'] = link
            yield item
        total = get_total(response.text)
        page = kwargs.get('page', 1)
        if total > page:
            page += 1
            url = kwargs.get('url')
            if not url:
                url = response.url
                paths = urlparse(url).path.strip('/').split('/')
                if len(paths) == 2:
                    url += '/'  # URL中只有一级分类时，页码前面必须加"/"
            yield scrapy.Request(
                url=f'{url}p{page}',
                callback=self.parse,
                dont_filter=True,
                cb_kwargs={'url': url, 'page': page}
            )


if __name__ == '__main__':
    from scrapy.cmdline import execute
    execute(['scrapy', 'crawl', 'shop_link'])
