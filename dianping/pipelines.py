# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import json
import os
from pathlib import Path

from itemadapter import ItemAdapter

from dianping.settings import DATA_DIR


class DianpingPipeline:
    """ 保存数据为JSON格式 """

    def __init__(self):
        self.file = None
        self.lines = 0

    def open_spider(self, spider):
        self.file = open(Path(DATA_DIR, spider.name + '.jsonl'), 'wb')

    def close_spider(self, spider):
        if self.lines > 0:
            # 删除末尾的换行符
            self.file.seek(-1, os.SEEK_END)
            self.file.truncate()
        self.file.close()

    def process_item(self, item, spider):
        line = json.dumps(ItemAdapter(item).asdict(), ensure_ascii=False) + "\n"
        self.file.write(line.encode('utf-8'))
        self.lines += 1
        return item
