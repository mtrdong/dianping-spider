import re
from contextlib import suppress
from io import BytesIO

import demjson3
import requests
from fontTools.ttLib import TTFont
from lxml import etree

from dianping.settings import WOFF_FONT

# WOFF字库
FONT_LIBRARY = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '店', '中', '美', '家', '馆', '小', '车', '大', '市',
                '公', '酒', '行', '国', '品', '发', '电', '金', '心', '业', '商', '司', '超', '生', '装', '园', '场', '食',
                '有', '新', '限', '天', '面', '工', '服', '海', '华', '水', '房', '饰', '城', '乐', '汽', '香', '部', '利',
                '子', '老', '艺', '花', '专', '东', '肉', '菜', '学', '福', '饭', '人', '百', '餐', '茶', '务', '通', '味',
                '所', '山', '区', '门', '药', '银', '农', '龙', '停', '尚', '安', '广', '鑫', '一', '容', '动', '南', '具',
                '源', '兴', '鲜', '记', '时', '机', '烤', '文', '康', '信', '果', '阳', '理', '锅', '宝', '达', '地', '儿',
                '衣', '特', '产', '西', '批', '坊', '州', '牛', '佳', '化', '五', '米', '修', '爱', '北', '养', '卖', '建',
                '材', '三', '会', '鸡', '室', '红', '站', '德', '王', '光', '名', '丽', '油', '院', '堂', '烧', '江', '社',
                '合', '星', '货', '型', '村', '自', '科', '快', '便', '日', '民', '营', '和', '活', '童', '明', '器', '烟',
                '育', '宾', '精', '屋', '经', '居', '庄', '石', '顺', '林', '尔', '县', '手', '厅', '销', '用', '好', '客',
                '火', '雅', '盛', '体', '旅', '之', '鞋', '辣', '作', '粉', '包', '楼', '校', '鱼', '平', '彩', '上', '吧',
                '保', '永', '万', '物', '教', '吃', '设', '医', '正', '造', '丰', '健', '点', '汤', '网', '庆', '技', '斯',
                '洗', '料', '配', '汇', '木', '缘', '加', '麻', '联', '卫', '川', '泰', '色', '世', '方', '寓', '风', '幼',
                '羊', '烫', '来', '高', '厂', '兰', '阿', '贝', '皮', '全', '女', '拉', '成', '云', '维', '贸', '道', '术',
                '运', '都', '口', '博', '河', '瑞', '宏', '京', '际', '路', '祥', '青', '镇', '厨', '培', '力', '惠', '连',
                '马', '鸿', '钢', '训', '影', '甲', '助', '窗', '布', '富', '牌', '头', '四', '多', '妆', '吉', '苑', '沙',
                '恒', '隆', '春', '干', '饼', '氏', '里', '二', '管', '诚', '制', '售', '嘉', '长', '轩', '杂', '副', '清',
                '计', '黄', '讯', '太', '鸭', '号', '街', '交', '与', '叉', '附', '近', '层', '旁', '对', '巷', '栋', '环',
                '省', '桥', '湖', '段', '乡', '厦', '府', '铺', '内', '侧', '元', '购', '前', '幢', '滨', '处', '向', '座',
                '下', '県', '凤', '港', '开', '关', '景', '泉', '塘', '放', '昌', '线', '湾', '政', '步', '宁', '解', '白',
                '田', '町', '溪', '十', '八', '古', '双', '胜', '本', '单', '同', '九', '迎', '第', '台', '玉', '锦', '底',
                '后', '七', '斜', '期', '武', '岭', '松', '角', '纪', '朝', '峰', '六', '振', '珠', '局', '岗', '洲', '横',
                '边', '济', '井', '办', '汉', '代', '临', '弄', '团', '外', '塔', '杨', '铁', '浦', '字', '年', '岛', '陵',
                '原', '梅', '进', '荣', '友', '虹', '央', '桂', '沿', '事', '津', '凯', '莲', '丁', '秀', '柳', '集', '紫',
                '旗', '张', '谷', '的', '是', '不', '了', '很', '还', '个', '也', '这', '我', '就', '在', '以', '可', '到',
                '错', '没', '去', '过', '感', '次', '要', '比', '觉', '看', '得', '说', '常', '真', '们', '但', '最', '喜',
                '哈', '么', '别', '位', '能', '较', '境', '非', '为', '欢', '然', '他', '挺', '着', '价', '那', '意', '种',
                '想', '出', '员', '两', '推', '做', '排', '实', '分', '间', '甜', '度', '起', '满', '给', '热', '完', '格',
                '荐', '喝', '等', '其', '再', '几', '只', '现', '朋', '候', '样', '直', '而', '买', '于', '般', '豆', '量',
                '选', '奶', '打', '每', '评', '少', '算', '又', '因', '情', '找', '些', '份', '置', '适', '什', '蛋', '师',
                '气', '你', '姐', '棒', '试', '总', '定', '啊', '足', '级', '整', '带', '虾', '如', '态', '且', '尝', '主',
                '话', '强', '当', '更', '板', '知', '己', '无', '酸', '让', '入', '啦', '式', '笑', '赞', '片', '酱', '差',
                '像', '提', '队', '走', '嫩', '才', '刚', '午', '接', '重', '串', '回', '晚', '微', '周', '值', '费', '性',
                '桌', '拍', '跟', '块', '调', '糕']


def replace_text(func):
    """ 替换HTML页面中的反爬文本 """
    def inner(html_content: str):
        # 检查HTML页面是否有字体反爬
        css_link = has_woff_font(html_content)
        if css_link and not WOFF_FONT:
            # 如果检测到网页有字体反爬，且字体解析字典为空
            # 则通过网页中的CSS获取字体文件，并解析成字典
            anal_woff_font(css_link)
        # 替换HTML中的反爬文字和数字
        for k, v in WOFF_FONT.items():
            html_content = html_content.replace(k, v)
        return func(html_content)

    return inner


@replace_text
def get_html_text(tag):
    """ 获取HTML标签中所有文本 """
    html = etree.HTML(tag)
    text = html.xpath('normalize-space(string(.))').replace(' ', '')
    return text


def remove_html_tag(html_element, tag, **kwargs):
    """ 去除HTML中指定的标签 """
    attrs = ' and '.join([f'@{k}="{v}"' for k, v in kwargs.items()])
    xpath_eval = attrs if attrs else ''
    for element in html_element.xpath(f'{tag}{xpath_eval}'):
        element.getparent().remove(element)


def is_login(html_content):
    """ 检查是否登录 """
    html = etree.HTML(html_content)
    scripts = html.xpath('//script/text()')
    for script in scripts:
        if 'window._DP_HeaderData' in script:
            result = re.findall(r"'userId': '([0-9]+)'", script)
            return bool(result)
    return False


def has_woff_font(html_content):
    """ 检查HTML页面是否有WOFF字体反爬
    有则返回CSS链接；无则返回False
    """
    css_link = re.findall(r'href="(//s3plus.meituan.*?)"', html_content)
    if css_link:
        return 'https:' + css_link[0]
    return False


def anal_woff_font(css_link):
    """ 从CSS文件中获取WOFF字体链接，并下载字体解析成字典 """
    # 下载CSS
    response = requests.get(css_link)
    content = response.content.decode()
    # 提取字体链接
    font_face_list = re.findall(r'@font-face\s*{([\s\S]*?)}', content)
    font_address_link = None
    font_num_link = None
    for font_face in font_face_list:
        if 'PingFangSC-Regular-address' in font_face:
            font_address_link = 'http:' + re.findall(r'url\("(\S*.woff)"\);', font_face)[0]
        elif 'PingFangSC-Regular-num' in font_face:
            font_num_link = 'http:' + re.findall(r'url\("(\S*.woff)"\);', font_face)[0]
        if font_address_link and font_num_link:
            break
    # 下载字体
    response = requests.get(font_address_link)
    font_address = response.content
    response = requests.get(font_num_link)
    font_num = response.content
    # 更新字体
    font_address_woff = TTFont(BytesIO(font_address))
    glyph_order = font_address_woff.getGlyphOrder()
    WOFF_FONT.update({f'&#x{glyph_order[i + 12].lstrip("uni")};': v for i, v in enumerate(FONT_LIBRARY[10:])})
    font_num_woff = TTFont(BytesIO(font_num))
    glyph_order = font_num_woff.getGlyphOrder()
    WOFF_FONT.update({f'&#x{glyph_order[i + 2].lstrip("uni")};': v for i, v in enumerate(FONT_LIBRARY[:10])})


def anal_header_data(html_content):
    """ 从页面中解析页首数据 """
    header_data = re.findall(r'window._DP_HeaderData\s*=\s*({[\s\S]*?})', html_content) or {}
    if header_data:
        header_data = demjson3.decode(header_data[0])
    return header_data


def anal_city_pinyin(html_content):
    """ 从更多城市页中解析城市拼音 """
    html = etree.HTML(html_content)
    elements = html.xpath('//div[@class="findHeight"]/a')
    data = {element.text: element.attrib.get('href').split('/')[-1] for element in elements}
    return data


def anal_cate1_id(html_content):
    """ 从首页中解析一级分类名称和代码 """
    html = etree.HTML(html_content)
    elements = html.xpath('//div[@id="nav"]//ul/li//a[@data-click-title="first"]')
    data = {element.text: element.attrib.get('data-click-name') for element in elements}
    return data


def anal_cate2_id(html_content):
    """ 从店铺列表页中解析二级分类名称和代码 """
    html = etree.HTML(html_content)
    elements = html.xpath('//div[@id="classfy"]/a')
    data = {element.xpath('span/text()')[0]: 'g' + element.xpath('@data-cat-id')[0] for element in elements}
    return data


def anal_cate3_id(html_content):
    """ 从店铺列表页中解析三级分类名称和代码 """
    html = etree.HTML(html_content)
    elements = html.xpath('//div[@id="classfy-sub"]/a')
    data = {
        element.xpath('span/text()')[0]: 'g' + element.xpath('@data-cat-id')[0]
        for element in elements
        if element.xpath('@data-cat-id')
    }
    return data


def get_total(html_content):
    """ 获取总页数 """
    html = etree.HTML(html_content)
    total_page = html.xpath('//div[@class="page"]/a[@class="PageLink" or @class="cur"]/text()')
    if total_page:
        return int(total_page[-1])
    return 0


def has_next(html_content):
    """ 检查是否有下一页 """
    html = etree.HTML(html_content)
    next_page = html.xpath('//div[@class="page"]/a[@class="next"]')
    if next_page:
        return True
    return False


def anal_shop_link(html_content):
    """ 从店铺列表页中解析店铺的链接 """
    html = etree.HTML(html_content)
    elements = html.xpath('//div[@id="shop-all-list"]/ul/li//a[@data-hippo-type="shop"]')
    data = {element.attrib.get('title'): element.attrib.get('href') for element in elements}
    return data


@replace_text
def anal_shop_info(html_content):
    """ 从店铺详情页中解析店铺的基本信息 """
    html = etree.HTML(html_content)
    texts = html.xpath('//script/text()')
    data = {}
    with suppress(TypeError):
        for text in texts:
            if 'facade' in text:
                facades = re.findall(r'facade\s*\(([\s\S]*?)\);', text)
                shop = [s for s in facades if 'app-shop-full-map' in s or 'app-shop-full-map-placeholder' in s]
                query = [s for s in facades if 'app-midas' in s or 'sideMkt' in s]
                if shop and query:
                    shop = demjson3.decode(shop[0])
                    query = demjson3.decode(query[0])
                    shop = shop.get('data')
                    query = query.get('data').get('query') or query.get('data').get('sideMkt').get('query')
                    data.update(
                        name=shop['shopName'],
                        address=shop['address'],
                        city=shop['cityCnName'],
                        ids={
                            'shopId': shop['shopId'],
                            'cityId': query['cityId'],
                            'mainCategoryId': query['categoryId']
                        }
                    )
            if 'window.shop_config' in text:
                shop_config = re.findall(r'window.shop_config\s*=\s*{([\s\S]*)}', text)[0]
                if shop_config:
                    shop_config = '{' + shop_config + '}'
                    shop_config = demjson3.decode(shop_config)
                    data.update(
                        name=get_html_text(shop_config['shopName']),
                        address=get_html_text(shop_config['address']),
                        city=shop_config['cityCnName'],
                        ids={
                            'shopId': shop_config['shopId'],
                            'cityId': shop_config['cityId'],
                            'mainCategoryId': shop_config['mainCategoryId']
                        }
                    )
    return data


def anal_review_star(json_data):
    """ 解析店铺的星级、评论数、费用和评分 """
    data = {
        'shop_power': int(json_data.get('shopPower')),  # 店铺星星
        'five_score': float(json_data.get('fiveScore')),  # 店铺星星分数
        'review_count': int(get_html_text(json_data.get('defaultReviewCount'))),  # 店铺评价数
        'avg_price': {
            'title': json_data.get('avgPriceTitle'),
            'price': int(get_html_text(json_data.get('avgPrice')))
        },  # 店铺消费价格
        'shop_score': None  # 店铺评分
    }
    score_title_list = json_data.get('shopScoreTitleList') or []
    score_value_list = json_data.get('shopRefinedScoreValueList') or []
    if score_title_list and score_value_list:
        score_title_list = [get_html_text(val) for val in json_data.get('shopScoreTitleList')]
        score_value_list = [get_html_text(val) for val in json_data.get('shopRefinedScoreValueList')]
        data['shop_score'] = {score_title_list[index]: float(value) for index, value in enumerate(score_value_list)}
    return data


@replace_text
def anal_trade_area(html_content):
    """ 从店铺详情页中解析热门商圈 """
    html = etree.HTML(html_content)
    elements_1 = html.xpath('//div[@class="tabs"]/a/@data-click-name')
    elements_2 = html.xpath('//div[@class="navigation-block clear-both"]')
    elements_3 = html.xpath('//dl[@class="site-map_house__tab js_sitemap-fold"]')
    word = '热门商圈'
    data = {}
    if elements_1:
        for index, value in enumerate(elements_1):
            if word in value:
                nc_items = html.xpath('//div[@class="con"]/div')
                if len(elements_1) == len(nc_items):
                    data['business_district'] = {}
                    nc_items = nc_items[index].xpath('a')
                    for item in nc_items:
                        data['business_district'].update({item.attrib.get('data-click-name'): item.attrib.get('href').lstrip('//')})
                break
    elif elements_2:
        for element in elements_2:
            title = element.xpath('div[@class="left-title"]/text()')
            if title and word in title[0]:
                data['business_district'] = {}
                right_list = element.xpath('div[@class="right-list"]//a[not(@class="right-item show-more")]')
                for item in right_list:
                    data['business_district'].update({item.text: 'www.dianping.com' + item.attrib.get('href')})
                break
    elif elements_3:
        for element in elements_3:
            text = element.xpath('dt/text()')
            if text and word in text[0]:
                data['business_district'] = {}
                cnt_items = element.xpath('dd//a')
                for item in cnt_items:
                    data['business_district'].update({item.text: item.attrib.get('href').lstrip('//')})
                break
    return data
