import json
from pathlib import Path

from dianping.settings import DATA_DIR


def float2hex(number: float, base: int = 16):
    """ 浮点数转十六进制 """
    sign, number = ('-', -number) if number < 0 else ('', number)
    sl = [sign + str(int(number)) + '.']
    number - int(number)

    for _ in range(base):
        i = int(number * 16)
        sl.append(hex(i)[2:])
        number = number * 16 - i

    return ''.join(sl).rstrip('0')


def load_json_data(filename):
    """ 加载JSON数据 """
    path = Path(DATA_DIR, filename)
    if path.is_file():
        with open(path, 'r', encoding='UTF-8') as file:
            while True:
                line = file.readline()
                if line:
                    yield json.loads(line)
                else:
                    break


if __name__ == '__main__':
    # 0.b12a062a7e501
    f = 0.6920474866622615
    print(float2hex(f))
