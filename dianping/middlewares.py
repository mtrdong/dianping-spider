# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html
import base64
import random
from urllib.parse import urlparse, urljoin

from DrissionPage import ChromiumPage, ChromiumOptions
from fake_useragent import UserAgent
from scrapy import signals, Request, Spider
from scrapy.downloadermiddlewares.cookies import CookiesMiddleware
from scrapy.downloadermiddlewares.redirect import RedirectMiddleware, _build_redirect_request
from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware
from scrapy.http import Response
from scrapy.settings import Settings
from w3lib.url import safe_url_string


class DianpingSpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, or item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request or item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class DianpingDownloaderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class CustomCookiesMiddleware(CookiesMiddleware):
    """ 自定义 Cookies 中间件。统一给 Spider 设置 Cookies """

    @classmethod
    def from_crawler(cls, crawler):
        middleware: CustomCookiesMiddleware = super().from_crawler(crawler)
        middleware.login(crawler.settings)
        return middleware

    def process_request(self, request: Request, spider: Spider):
        """ 请求处理: 设置 cookies """
        cookies = spider.settings.get('COOKIES')
        for key, val in cookies.items():
            request.cookies.setdefault(key, val)
        super().process_request(request, spider)

    @staticmethod
    def login(settings: Settings):
        """ 登录 """
        # 创建页面对象并打开登录页面
        options = ChromiumOptions()
        options.set_paths(browser_path=settings.get('BINARY_LOCATION'))
        page = ChromiumPage(options)
        page.get(settings.get('LOGIN_URL'))
        # 等待登录完成
        while True:
            ele = page.ele('xpath://span[@class="userinfo-container"]', timeout=1)
            if ele:
                settings.frozen = False
                settings.set('COOKIES', page.cookies)
                settings.freeze()
                break
        # 关闭页面
        page.quit()


class CustomUserAgentMiddleWare(UserAgentMiddleware):
    """ 自定义 User-Agent 中间件。给 Spider 设置随机 UA"""

    def process_request(self, request: Request, spider: Spider):
        """ 请求处理: 设置随机 UA """
        ua = UserAgent()
        request.headers.update({'User-Agent': ua.random})


class CustomRedirectMiddleware(RedirectMiddleware):
    """ 自定义 Redirect 中间件
    触发验证时启动浏览器并转到验证页面进行验证，而不是直接重定向到验证链接
    """

    def process_response(self, request: Request, response: Response, spider: Spider):
        """ 响应处理
        1. 发生重定向时，不直接返回会响应对象（特殊情况除外），而是创建一个新的请求对象对重定向链接发起请求
        2. 如果重定向链接为验证链接，则启动浏览器转到验证页面进行验证，然后获取新的 Cookies，并将重定向链接修改为原始链接
        3. 如果重定向链接非验证链接，则重定向链接保持不变
        """
        if (
                request.meta.get('dont_redirect', False)
                or response.status in getattr(spider, 'handle_httpstatus_list', [])
                or response.status in request.meta.get('handle_httpstatus_list', [])
                or request.meta.get('handle_httpstatus_all', False)
        ):
            return response

        allowed_status = (301, 302, 303, 307, 308)
        if 'Location' not in response.headers or response.status not in allowed_status:
            return response

        location = safe_url_string(response.headers['Location'])
        if response.headers['Location'].startswith(b'//'):
            request_scheme = urlparse(request.url).scheme
            location = request_scheme + '://' + location.lstrip('/')

        redirected_url = urljoin(request.url, location)

        settings = spider.settings
        if settings.get('VERIFY_URL') in redirected_url:
            # 如果重定向链接为验证链接，则不重定向到验证链接
            # 而是启动浏览器转到验证页面进行验证，并获取新的 Cookies，然后继续请求原始链接
            # 目的是防止爬取数据过程中触发验证时打断数据爬取
            self.verify(redirected_url, request, settings)
            redirected_url = request.url

        if response.status in (301, 307, 308) or request.method == 'HEAD':
            redirected = _build_redirect_request(request, url=redirected_url)
            return self._redirect(redirected, request, spider, response.status)

        redirected = self._redirect_request_using_get(request, redirected_url)
        return self._redirect(redirected, request, spider, response.status)

    @staticmethod
    def verify(url: str, request: Request, settings: Settings):
        """ 过验证 """
        # 创建页面对象并打开验证页面
        options = ChromiumOptions()
        options.set_paths(browser_path=settings.get('BINARY_LOCATION'))
        options.set_user_agent(request.headers['User-Agent'].decode(request.headers.encoding))
        page = ChromiumPage(options)
        page.get(url)
        # 等待验证完成
        while True:
            ele = page.ele('xpath://span[@class="userinfo-container"]', timeout=1)
            if ele:
                settings.frozen = False
                settings.set('COOKIES', page.cookies)
                settings.freeze()
                break
        # 关闭页面
        page.quit()


class ProxyMiddleware:
    """ 代理设置中间件。给 Spider 设置随机代理 """

    def process_request(self, request: Request, spider: Spider):
        """ 请求处理: 设置随机代理 """
        proxy_list = spider.settings.get('PROXY_LIST')
        if proxy_list:
            proxy = random.choice(proxy_list)
            request.meta['proxy'] = proxy['proxy']
            if proxy.get('username') and proxy.get('password'):
                request.headers['Proxy-Authorization'] = self.get_proxy_auth(proxy)

    @staticmethod
    def get_proxy_auth(proxy):
        auth = f"{proxy['username']}:{proxy['password']}"
        encoded_auth = base64.b64encode(auth.encode('utf-8')).decode('utf-8')
        return 'Basic ' + encoded_auth
